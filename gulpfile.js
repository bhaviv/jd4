var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');


var config = {
  themeFolder: './web/app/themes/jd1' 
}

config['sass'] = {
  sourceFolder: config.themeFolder + '/sass',
  sourceFiles: config.themeFolder + '/sass/**/*.scss',
  destFolder: config.themeFolder + '/css'
}

gulp.task('sass', function () {
  return gulp.src(config.sass.sourceFiles)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(config.sass.destFolder))
    .pipe(livereload())
});
 
gulp.task('sass:watch', function () {
  livereload.listen();
  gulp.watch(config.sass.sourceFiles, ['sass']);
});

gulp.task('livereload', function () {
  livereload.listen();
  gulp.watch(config.themeFolder + '/**/*', function () {
  return gulp.src(config.themeFolder + '/**/*')
    .pipe(livereload())
  });
});

gulp.task('default', ['sass:watch', 'livereload'])
