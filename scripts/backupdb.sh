PROJECT_DIR=`dirname $0`/..
echo $PROJECT_DIR/.env
. $PROJECT_DIR/.env
echo $DB_NAME
backupFolder=$PROJECT_DIR/backups
backupFile=$backupFolder/$DB_NAME.`date '+%Y-%m-%d-%H-%M'`.sql
mysqldump --login-path=local --databases $DB_NAME > $backupFile 
gzip $backupFile
ls -la $backupFolder

